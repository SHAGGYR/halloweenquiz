﻿using System;

namespace HalloweenQuiz
{
    class Program
    {
        static void Main()
        {
            var questions = new[]
            {
                new HalloweenQuestion("What is the word Hallowe’en an abbreviation of?",
                    new[]
                        {new Answer("All Hallows’ Eve", true), new Answer("Saints' Eve"), new Answer("Hollows Eve")}),

                new HalloweenQuestion("What ancient Celtic festival did Halloween originate from?",
                    new[]
                        {new Answer("Summer's End", true), new Answer("Autumn's End"), new Answer("Winter's End")}),

                new HalloweenQuestion("What would you traditionally bob for at Halloween parties?",
                    new[]
                        {new Answer("Apples", true), new Answer("Pumpkins"), new Answer("Oranges")}),

                new HalloweenQuestion("What were Jack O’Lanterns carved from before pumpkins were used?",
                    new[]
                        {new Answer("Turnips", true), new Answer("Parsnips"), new Answer("Broccoli")}),

                new HalloweenQuestion("When were Halloween greetings cards first made?",
                    new[]
                        {new Answer("1900s", true), new Answer("1800s"), new Answer("1700s")}),

                new HalloweenQuestion("What is a group of witches called?",
                    new[]
                        {new Answer("A coven", true), new Answer("A clover"), new Answer("A cloven")}),

                new HalloweenQuestion("Some people claim to be real vampires: true or false?",
                    new[]
                        {new Answer("True", true), new Answer("False")}),

                new HalloweenQuestion("What does the old English word 'Hallow' mean?",
                    new[]
                        {new Answer("Saint", true), new Answer("Devil"), new Answer("Angel")}),

                new HalloweenQuestion(
                    "What is the traditional Scottish name for Hallowe'en, which derives from the name of Celtic God Samana?",
                    new[]
                        {new Answer("Samhain", true), new Answer("Summan"), new Answer("Samson")}),

                new HalloweenQuestion("What is the day after Hallowe’en called?",
                    new[]
                    {
                        new Answer("All Souls' Day", true), new Answer("All Saints' Day"),
                        new Answer("All Sinners' Day")
                    }),

                new HalloweenQuestion("What do pumpkins grow on?",
                    new[]
                        {new Answer("Vines", true), new Answer("Trees"), new Answer("Stalks")}),

                new HalloweenQuestion("What was Dr. Frankenstein’s first name?",
                    new[]
                        {new Answer("Victor", true), new Answer("Frank"), new Answer("Fred")}),

                new HalloweenQuestion("Where do real vampire bats live?",
                    new[]
                        {new Answer("North and South America", true), new Answer("Europe"), new Answer("Asia")}),

                new HalloweenQuestion("Where did bobbing for apples originate?",
                    new[]
                        {new Answer("Ancient Rome", true), new Answer("Greece"), new Answer("Dublin")}),

                new HalloweenQuestion("Where does 'trick-or-treating' come from?",
                    new[]
                    {
                        new Answer("Souling and guising", true), new Answer("Hiding and seeking"),
                        new Answer("Hunting and preying ")
                    }),
            };
            var quiz = Quiz.GetInstance(questions);
            quiz.DisplayRandomQuestion();
            Console.ReadKey(true);
        }
    }
}