﻿namespace HalloweenQuiz
{
    public struct Answer
    {
        public Answer(string answer, bool isTrue = false)
        {
            AnswerText = answer;
            IsTrue = isTrue;
        }

        public string AnswerText { get; }
        public bool IsTrue { get; }
    }
}