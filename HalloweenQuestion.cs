﻿namespace HalloweenQuiz
{
    public struct HalloweenQuestion
    {
        public HalloweenQuestion(string question, Answer[] answers)
        {
            Question = question;
            Answers = answers;
        }

        public string Question { get; }
        public Answer[] Answers { get; set; }
    }
}