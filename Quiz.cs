﻿using System;
using System.Collections.Generic;

namespace HalloweenQuiz
{
    public class Quiz
    {
        private static Quiz _instance;

        private readonly List<int> _unseenQuestions;

        private readonly Random _randomizer;

        private const string End = "Thanks for participating. All for now. Press escape to get your prize.";
        private const string WellDone = "Well done.";
        private const string Choose = "Choose your answer or press escape to finish.";
        private const string NiceTry = "You was close. Wanna try again, go next or finish? {a/n/esc}";

        private readonly HalloweenQuestion[] _questions;

        private Quiz(HalloweenQuestion[] questions)
        {
            _questions = questions;
            _unseenQuestions = new List<int>();
            _randomizer = new Random();
            for (var i = 0; i < _questions.Length; i++)
            {
                _unseenQuestions.Add(i);
            }
        }

        public static Quiz GetInstance(HalloweenQuestion[] questions)
        {
            return _instance ?? (_instance = new Quiz(questions));
        }

        public void DisplayRandomQuestion()
        {
            if (_unseenQuestions.Count == 0)
            {
                Console.WriteLine(End);
                return;
            }

            var question = GetRandomQuestion();

            Console.WriteLine($"Your question: {question.Question}");
            for (var i = 0; i < question.Answers.Length; i++)
            {
                Console.Write($"{i + 1}): {question.Answers[i].AnswerText}\t");
            }

            Console.WriteLine();
            CheckAnswer(question);
        }

        private void CheckAnswer(HalloweenQuestion question)
        {
            int correctAnswerId = -1;
            for (var i = 0; i < question.Answers.Length; i++)
            {
                if (!question.Answers[i].IsTrue) continue;
                correctAnswerId = i;
                break;
            }

            CheckUserInput(correctAnswerId);
        }

        private void CheckUserInput(int correctAnswer)
        {
            Console.WriteLine(Choose);
            var input = Console.ReadKey().Key;
            Console.WriteLine();
            if (((int) input == correctAnswer + 49) || ((int) input == correctAnswer + 97))
            {
                Console.WriteLine(WellDone);
                DisplayRandomQuestion();
            }
            else if (input == ConsoleKey.Escape)
            {
                Console.WriteLine(End);
            }
            else
            {
                Console.WriteLine(NiceTry);
                input = Console.ReadKey().Key;
                Console.WriteLine();

                switch (input)
                {
                    case ConsoleKey.Escape:
                        Console.WriteLine(End);
                        break;
                    case ConsoleKey.N:
                        DisplayRandomQuestion();
                        break;
                    default:
                        CheckUserInput(correctAnswer);
                        break;
                }
            }
        }

        private void ShuffleAnswers(int questionId)
        {
            var numberOfAnswers = (_questions[questionId].Answers).Length;
            var shuffledAnswers = new Answer[numberOfAnswers];
            var index = _randomizer.Next(numberOfAnswers);
            var randomNumber = _randomizer.Next(2, numberOfAnswers);
            var shuffleConstructor = numberOfAnswers % 2 == 0
                ? randomNumber % 2 == 0
                    ? (randomNumber + 1) % numberOfAnswers
                    : randomNumber
                : randomNumber % 2 == 0
                    ? randomNumber
                    : (randomNumber + 1) % numberOfAnswers;
            for (var i = 0; i < numberOfAnswers; i++)
            {
                shuffledAnswers[i] = (_questions[questionId].Answers[index]);
                index = (index + shuffleConstructor) % numberOfAnswers;
            }

            _questions[questionId].Answers = shuffledAnswers;
        }

        private HalloweenQuestion GetRandomQuestion()
        {
            var questionId = _unseenQuestions[_randomizer.Next(_unseenQuestions.Count)];
            _unseenQuestions.Remove(questionId);
            ShuffleAnswers(questionId);
            return _questions[questionId];
        }
    }
}